package com.example.rss_205150407111003;

import android.util.Log;

import com.example.rss_205150407111003.RssItem;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class RssParser {
    private static String TAG_ITEM = "item";
    private static String TAG_TITLE = "title";
    private static String TAG_CHANNEL = "channel";
    private static String TAG_LINK = "link";
    private static String TAG_DESCRIPTION = "description";

    public String loadRssFromUrl(String url) throws IOException {
        Log.d("RSSPARSER", "Start rss parser");
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = client.newCall(request).execute();
        String xml = response.body().string();
        Log.d("RSSPARSER", "xml " + xml);
        return xml;
    }

    public ArrayList<RssItem> parseRssFromUrl(String xml) throws ParserConfigurationException, IOException, SAXException {
        ArrayList<RssItem> list = new ArrayList<>();
        DocumentBuilderFactory builder = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = builder.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(xml));
        Document doc = db.parse(is);
        NodeList nodeList = doc.getElementsByTagName(TAG_CHANNEL);
        Element e = (Element) nodeList.item(0);

        NodeList items = e.getElementsByTagName(TAG_ITEM);
        for (int i = 0; i < items.getLength(); i++) {
            RssItem item = new RssItem();
            Element e1 = (Element) items.item(i);
            String judul = getNodeValue(e1,TAG_TITLE);
            Log.d("RSSPARSER", judul);
            item.TAG_TITLE = judul;
            list.add(item);

            String link = getNodeValue(e1,TAG_LINK);
            Log.d("RSSPARSER",link);
            item.TAG_LINK = link;

            String gambar = getNodeValue(e1,TAG_DESCRIPTION);
            item.TAG_IMAGE = gambar.substring(gambar.indexOf("src=")+5,gambar.indexOf("width")+5);
            String replace = item.TAG_IMAGE.replace("\" width","");
            Log.d("RSSPARSER",replace);
            item.TAG_IMAGE = replace;
        }
        return list;
    }
    public String getNodeValue(Element e1, String tag){
        NodeList n = e1.getElementsByTagName(tag);
        Node ne = n.item(0);
        Node child = ne.getFirstChild();
        return child.getNodeValue();
    }
}
