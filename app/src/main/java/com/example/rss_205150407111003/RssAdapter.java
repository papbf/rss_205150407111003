package com.example.rss_205150407111003;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RssAdapter extends RecyclerView.Adapter<RssAdapter.RssViewHolder> {
    LayoutInflater inflater;
    Context _context;
    ArrayList<RssItem> data;

    public RssAdapter(Context _context, ArrayList<RssItem> data){
        this._context = _context;
        this.data = data;
        this.inflater = LayoutInflater.from(this._context);
    }

    @NonNull
    @Override
    public RssAdapter.RssViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row,parent,false);
        return new RssViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RssAdapter.RssViewHolder holder, int position) {
        RssItem rssItem = data.get(position);
        holder.tvJudul.setText(data.get(position).TAG_TITLE);
        String gambar = rssItem.TAG_IMAGE.replace("localhost","10.0.2.2");
        PicassoClient.downloadImage(_context,gambar,holder.imageView);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class RssViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tvJudul;
        ImageView imageView;

        public RssViewHolder(@NonNull View itemView){
            super(itemView);
            tvJudul = itemView.findViewById(R.id.tvJudul);
            imageView = itemView.findViewById(R.id.imageView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view){
            int position = getAdapterPosition();
            Intent in = new Intent();
            in.setAction(Intent.ACTION_VIEW);
            in.addCategory(Intent.CATEGORY_BROWSABLE);
            in.setData(Uri.parse(data.get(position).TAG_LINK));
            _context.startActivity(in);
        }
    }

}
